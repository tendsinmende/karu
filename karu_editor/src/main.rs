//!# Karu Editor
//!
//! Karu's Editor is only a framework that does nothing else then creating a graphical context (the graph editor).
//! This involves listening for input and directing the input to the correct node, as well as handling state synchronistation based on the given connections.
//! Everything else (audio device creation, or network communication etc.) must be implemented in the nodes.
//!
//! However, some things are already programmed in the common `karu` crate. Stuff like audio buffers, and several other helper structures.
use editor::Editor;
use nakorender::{backend::Backend, winit::{event::{ElementState, Event, KeyboardInput, VirtualKeyCode, WindowEvent}, event_loop::ControlFlow}};
mod editor;

fn main(){
    //Create rendering context
    let eventloop = nakorender::winit::event_loop::EventLoop::new();
    let window = nakorender::winit::window::WindowBuilder::new()
        .with_title("Karu")
        //.with_window_icon() TODO: Create a icon for karu.
        .build(&eventloop).expect("Failed to create karus window.");
    let mut renderer = nakorender::marp::MarpBackend::new(&window, &eventloop);

    //Setup editor
    let mut editor = Editor::new(&window, &mut renderer);
    
    //The outer controll loop only enusres that the programm ends on the right condition. All other event processing
    //is managed inside the editor itself.
    eventloop.run(move |event, _target, control_flow|{
        //Set to polling for now, might be overwritten
        //TODO: Maybe we want to use "WAIT" for the ui thread? However, the renderers don't work that hard
        //if nothing changes. So should be okay for a alpha style programm.
        *control_flow = ControlFlow::Poll;
        
        editor.event(&mut renderer, &event);

        if editor.should_end(){
            *control_flow = ControlFlow::Exit;
        }
        
        //now check if a rerender was requested, or if we worked on all
        //events on that batch
        match event{
            Event::MainEventsCleared => {
                window.request_redraw();
            }
            Event::RedrawRequested(_) => {
                renderer.render(&window);
            }
            Event::WindowEvent{window_id: _, event: WindowEvent::KeyboardInput{
                device_id: _,
                input: KeyboardInput{
                    modifiers: _,
                    scancode: _,
                    state: ElementState::Released,
                    virtual_keycode: Some(VirtualKeyCode::Escape)
                },
                is_synthetic: _}} => {
                *control_flow = ControlFlow::Exit;
            }
            _ => {},
        }
        
    })
}
