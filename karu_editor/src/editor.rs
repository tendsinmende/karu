use nako::{glam::{Vec2, Vec3}, operations::{planar::primitives2d::Box2d, volumetric::{Color, Union}}, stream::{PrimaryStream2d, SecondaryStream2d}};
use nako_std::Area;
use nakorender::{backend::{Backend, LayerId2d, LayerInfo}, camera::Camera2d, winit::{event::WindowEvent, window::Window}};
//Reexport some stuff that nodes might need from time to time
pub use nakorender::winit::event::Event;


pub struct Editor{
    window_extent: Vec2,
    ///the main background layer of the app. Only changes on resize
    bg_main: LayerId2d,
    ///Main windows spacer layer, basically eyecandy
    bg_spacer: [LayerId2d; 3], //[spacer_left, spacer-toolbar-to-tabs, spacer-toolbar-to-editor]
}

fn setup_opaque_in_range(id: LayerId2d, renderer: &mut impl Backend, mut from: Vec2, mut to: Vec2, color: Color){
    if from.x > to.x{
        core::mem::swap(&mut from.x, &mut to.x);
    }
    if from.y > to.y{
        core::mem::swap(&mut from.y, &mut to.y);
    }
    
    let ext = to - from;
    println!("\n----\nExt: {}\n-----\n", ext);
    renderer.set_layer_info(
        id.into(),
        LayerInfo {
            location: (from.x as usize, from.y as usize),
            extent: (ext.x as usize, ext.y as usize),
        } //NOTE we use application wide the convention that the layers extent is synonymouse with the pixel width/height
    );
    
    renderer.update_sdf_2d(
        id,
        PrimaryStream2d::new().push(
            SecondaryStream2d::new(
                Union,
                Box2d{extent: ext}
            ).push_mod(color).build()
        ).build()
    );

    renderer.update_camera_2d(id, Camera2d{
        extent: ext,
        location: Vec2::ZERO,
        rotation: 0.0
    });
}

impl Editor{
    pub fn new(window: &Window, renderer: &mut impl Backend) -> Self{
        let window_ext = (window.inner_size().width as usize, window.inner_size().height as usize);
        let mut window_ext_vec = Vec2::new(window_ext.0 as f32, window_ext.1 as f32);

        //Sanitize window ext to not get one with ext == (0,0)
        if window_ext_vec == Vec2::ZERO{
            window_ext_vec = Vec2::new(1.0, 1.0);
        }
        //Now Setup the spacers
        
        let mut editor = Editor{
            window_extent: window_ext_vec,
            bg_main: renderer.new_layer_2d(),
            bg_spacer: [
                renderer.new_layer_2d(),
                renderer.new_layer_2d(),
                renderer.new_layer_2d(),
            ]
        };

        editor.update_bg(renderer);

        renderer.set_layer_order(&[editor.bg_main.into(), editor.bg_spacer[0].into(), editor.bg_spacer[1].into(), editor.bg_spacer[2].into()]);
        
        editor
    }

    fn context_spacer_area(&self) -> Area{
        //Currently hardcoded, will be changed
        let from = (
                0.0,
                (50.0f32).min(self.window_extent.y)
            ).into();
        Area{
            from,
            to: (
                (self.window_extent.x / 10.0).max(from.x + 1.0),
                self.window_extent.y.max(from.y + 1.0)
            ).into()
        }
    }
    
    //Updates inner layers based on the current window extent
    fn update_bg(&mut self, renderer: &mut impl Backend){
        //Setup bg
        setup_opaque_in_range(
            self.bg_main,
            renderer,
            Vec2::ZERO,
            self.window_extent,
            Color(Vec3::new(0.2, 0.2, 0.2))//TODO make part of config
        );

        //Now update spacers
        let left = self.context_spacer_area();
        setup_opaque_in_range(
            self.bg_spacer[0],
            renderer,
            left.from,
            left.to,
            Color(Vec3::new(0.5, 0.5, 0.5))//TODO make part of config
        );
    }

    pub fn should_end(&self) -> bool{
        false
    }
    
    pub fn event(&mut self, renderer: &mut impl Backend, event: &Event<()>){
        match event{
            Event::WindowEvent{window_id: _, event: WindowEvent::Resized(new_size)} => {
                self.window_extent = Vec2::new(new_size.width as f32, new_size.height as f32);
                if self.window_extent.min_element() == 0.0{
                    self.window_extent = Vec2::ONE;
                }
                println!("\n---\nResize to {:?}:\n---", new_size);
                self.update_bg(renderer);
            },
            _ => {}
        }
    }
    
    pub fn update_renderer(&mut self, _renderer: &mut impl Backend){
        
    }
}
