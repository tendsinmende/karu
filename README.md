# Karu

Node based DAW (3rd) attempt. This time with better rendering capabilities.

## Principles
Karu is build under the assumption that digital music and media production should allow a creator to easily automate the tools that are used.
Since programming via text tools is a quiet abstract thing to do, a better representation is needed. 
Karu arranges all prebuilt modules in a graph. The modules are connected via wires to visualize data paths. Note that the modules them self still have to be build via traditional rust programming.

## Modules
- karu: Base crate containing building blocks used for programmaticaly connecting and writing karu modules.

- karu-editor: Executable that opens an editor and hosts the karu modules



## Building and running
´cargo run --release´ should suffice on most linux based systems. If the editor does not start, or you can't hear anything, please open a issue!

## License
The whole project is licensed under MPL v2.0, all contributions will be licensed the same.

