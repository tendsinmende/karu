/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
use std::{collections::HashMap, fmt::Display, sync::{Arc, atomic::{AtomicBool, Ordering}}};

#[derive(Debug)]
pub enum MatrixError{
    SlotOutOfBounds,
    ///Whenever disconnect(a,b) a==b
    CanNotDisconnectSelf,
    ///If the ID did not exist
    IdDitNotExist(WeakNodeId),
}

struct ConnectionBit(Arc<AtomicBool>);
impl ConnectionBit{
    fn set(&self){
        (*self.0).store(true, Ordering::Relaxed);
    }
    
    fn unset(&self){
        (*self.0).store(false, Ordering::Relaxed);
    }

    fn get(&self) -> bool{
        (*self.0).load(Ordering::Relaxed)
    }
}

pub struct NodeId{
    pub(crate) location: usize,
    //Alive bit of this node
    bit: ConnectionBit 
}

impl NodeId{
    pub fn as_weak(&self) -> WeakNodeId{
        WeakNodeId(self.location)
    }
}

impl Display for NodeId{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "NodeId({})", self.location)
    }
}

impl Drop for NodeId{
    fn drop(&mut self) {
        //When dropping the id, mark all connections to this one unused
        self.bit.unset();
    }
}

///A node id that does not have the guarantee to exist within a graph.
#[derive(Clone, Copy, Hash, PartialEq, Eq, Debug)]
pub struct WeakNodeId(pub usize);


struct MTripel{
    //References to parent 0's connection bit
    p0: ConnectionBit,
    //References to parent 1's connection bit
    p1: ConnectionBit,
    //Bit, signaling if p0 and p1 are connected.
    are_connected: ConnectionBit,
}
impl MTripel{
    fn is_connected(&self) -> bool{
        self.p0.get() && self.p1.get() && self.are_connected.get()
    }
}

///The connection matrix saves a shareable, mutable bit for every connection x/y, where the bit is set, if the connection persists.
///
///The graph is bidirectional, always. Therefore we can save only the "lower" half of the table. Whenever a connection x/y is accessed where x>y, y/x should be accessed instead.
pub struct ConnectionMatrix{
    ///Table holding the bits indicating if a connection between x/y upholds
    table: Vec<MTripel>,
    node_table: Vec<ConnectionBit>,
}

impl Display for ConnectionMatrix{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut jump = 1;
        let mut idx = 0;

        write!(f, "|   |")?;
        for i in 0..self.node_table.len(){
            write!(f, " {} |", i)?;
        }
        write!(f, "\n")?;
        

        while idx < self.table.len(){
            let line_end = idx + jump;
            write!(f, "| {} |", jump - 1)?;
            while idx < line_end{
                write!(f, " {} |", if self.table[idx].is_connected(){"x"}else{" "})?;
                idx += 1;
            }
            write!(f, "\n")?;

            jump += 1;
        }

        Ok(())
    }
}

impl ConnectionMatrix{
    pub fn new() -> Self{
        ConnectionMatrix{
            table: Vec::new(),
            node_table: Vec::new(),
        }
    }

    pub fn with_capacity(capacity: usize) -> Self{
        let table = Vec::with_capacity(Self::access_idx(capacity));
        let node_table = Vec::with_capacity(capacity);
        let mut tmp = ConnectionMatrix{
            node_table,
            table
        };

        //Now "add" all nodes, then drop them again. for the first n alloc the caller should walk the "already allocated" path of "Self::alloc()"
        let ids: Vec<NodeId> = (0..capacity).map(|_i| tmp.alloc()).collect();
        drop(ids);
        tmp
    }

    ///Calculates the idx you have to access in the table, given the node number.
    #[inline]
    fn access_idx(node_number: usize) -> usize{
        let mut idx = 0;
        let mut jump_size = 2;
        for _ in 0..node_number{
            idx += jump_size;
            jump_size += 1;
        }

        idx
    }

    #[inline]
    fn access2d(mut a: usize, mut b: usize) -> usize{
        if a > b{
            core::mem::swap(&mut a, &mut b);
        }

        let b_idx = Self::access_idx(b);
        //Since we found the idx for b/b, now move back to a/b. a<b is statisfied above.
        b_idx - (b-a)
    }
    
    ///Searches for an unused slot in the connection table. Unsed slots are the ones, where the last bit on its row is unset.
    pub fn alloc(&mut self) -> NodeId{
        debug_assert!(self.validate());

        for (i, bit) in self.node_table.iter().enumerate(){
            if !bit.get(){

                //Recycle state by unsetting all edges, the setting our own edge
                for sub_i in 0..self.node_table.len(){
                    self.table[Self::access2d(i, sub_i)].are_connected.unset();
                }
                self.table[Self::access_idx(i)].are_connected.set();
                self.node_table[i].set(); //finaly set node active again
                
                return NodeId{
                    location: i,
                    bit: ConnectionBit(self.node_table[i].0.clone())
                };
            }
        }

        //Did not find a node, therefore allocate a new line
        self.node_table.push(ConnectionBit(Arc::new(AtomicBool::new(true))));
        self.table.reserve(self.node_table.len());
        for i in 0..self.node_table.len(){
            //Init all those connections in the table
            self.table.push(MTripel{
                are_connected: if i == (self.node_table.len() - 1){
                    ConnectionBit(Arc::new(AtomicBool::new(true))) //is self referencing table
                }else{
                    ConnectionBit(Arc::new(AtomicBool::new(false))) //assume no connection for all others
                },
                p0: ConnectionBit(self.node_table[i].0.clone()),
                p1: ConnectionBit(self.node_table[self.node_table.len() - 1].0.clone()) //second is always the newly created bit
            })
        }
        
        debug_assert!(self.validate());
        let location = self.node_table.len() - 1;
        NodeId{
            location,
            bit: ConnectionBit(self.node_table[location].0.clone())
        }
    }

    pub fn weak_exists(&self, a: WeakNodeId) -> bool{
        Self::access_idx(a.0) < self.table.len() && self.table[Self::access_idx(a.0)].is_connected()
    }
    
    pub fn connect(&mut self, a: &NodeId, b: &NodeId) -> Result<(), MatrixError>{
        if a.location == b.location{
            return Ok(());//are always connect
        }
        
        let conslot = Self::access2d(a.location, b.location);
        debug_assert!(conslot < self.table.len());//Shouldnt be possible, since the Nodeids are always valid
        self.table[conslot].are_connected.set(); //mark as connected now
        Ok(())
    }

    pub fn weak_disconnect(&mut self, a: &WeakNodeId, b: &WeakNodeId) -> Result<(), MatrixError>{
        if !self.weak_exists(*a){
            return Err(MatrixError::IdDitNotExist(*a));
        }
        if !self.weak_exists(*b){
            return Err(MatrixError::IdDitNotExist(*b));
        }

        let conslot = Self::access2d(a.0, b.0);
        debug_assert!(conslot < self.table.len());//Should not happen. strong ids are always valid
        self.table[conslot].are_connected.unset(); //mark as unset
        Ok(())
    }
    
    pub fn disconnect(&mut self, a: &NodeId, b: &NodeId) -> Result<(), MatrixError>{
        if a.location == b.location{
            return Err(MatrixError::CanNotDisconnectSelf);
        }

        let conslot = Self::access2d(a.location, b.location);
        debug_assert!(conslot < self.table.len());//Should not happen. strong ids are always valid
        self.table[conslot].are_connected.unset(); //mark as unset
        Ok(())
    }

    pub fn neighbors<'a>(&'a self, node: &NodeId) -> IterNeighbors<'a>{
        IterNeighbors{
            matrix: self,
            node_pos: node.location,
            pos: 0
        }
    }

    pub fn subgraph<'a>(&'a self, node: &NodeId) -> SubGraphIter<'a>{
        let mut candidates = HashMap::new();
        candidates.insert(node.as_weak(), false);
        SubGraphIter{
            matrix: self,
            candidates
        }
    }

    pub fn weak_neighbors<'a>(&'a self, node: &WeakNodeId) -> Option<IterNeighbors<'a>>{
        if !self.weak_exists(*node){
            println!("{} does not exist", node.0);
            return None;
        }
        Some(IterNeighbors{
            matrix: self,
            node_pos: node.0,
            pos: 0
        })
    }

    pub fn weak_subgraph<'a>(&'a self, node: &WeakNodeId) -> Option<SubGraphIter<'a>>{
        if !self.weak_exists(*node){
            return None;
        }
        let mut candidates = HashMap::new();
        candidates.insert(*node, false);
        Some(SubGraphIter{
            matrix: self,
            candidates
        })
    }

    ///Checks if a and b are in the same subgraph
    pub fn in_same_subgraph(&self, a: &NodeId, b: &NodeId) -> bool{
        //We check that by iterating over a's subgraph, and returning true if we found b
        let b = b.as_weak();
        for n in self.subgraph(a){
            if n == b{
                return true
            }
        }
        false
    }
    
    pub fn validate(&self) -> bool{
        if self.table.len() == 0{
            //empty table can't be invalid
            return true;
        }
        
        if Self::access_idx(self.node_table.len() - 1) != self.table.len() - 1{
            println!("self.size and table.len() do not match: {} != {}", Self::access_idx(self.node_table.len() - 1), self.table.len() - 1);
            return false;
        }

        true
    }
}

///Iterator that iterates over all neighbors of some node. Yielding the WeakNodeId.
pub struct IterNeighbors<'a>{
    matrix: &'a ConnectionMatrix,
    pos: usize,
    node_pos: usize
}

impl<'a> Iterator for IterNeighbors<'a>{
    type Item = WeakNodeId;
    fn next(&mut self) -> Option<Self::Item> {
        loop{
            let access_idx = ConnectionMatrix::access2d(self.pos, self.node_pos);
            if access_idx >= self.matrix.table.len(){
                return None;
            }

            //Gotta iter now
            let yield_tupel = (self.matrix.table[access_idx].is_connected(), self.pos);
            self.pos += 1;
            //If there is a connection, and it is not "self", yield it, otherwise try again with the moved pos
            if yield_tupel.0 && yield_tupel.1 != self.node_pos{
                return Some(WeakNodeId(yield_tupel.1));
            }
        }
    }
}


///Iterator that iterates over all reachable node, starting from some start node. The search is breadth first search-y. So allocation while searching might happen.
pub struct SubGraphIter<'a>{
    matrix: &'a ConnectionMatrix,
    ///Candidates that have been searched, and that should still be searched
    candidates: HashMap<WeakNodeId, bool>,
    
}

impl<'a> Iterator for SubGraphIter<'a>{
    ///A reachable node in this subgraph.
    type Item = WeakNodeId;
    fn next(&mut self) -> Option<Self::Item> {
        
        let c = self.candidates.iter().find(|e| if !e.1{true}else{false});
        
        if let Some(candidate) = c{
            let candidate = (*candidate.0, *candidate.1);
            //mark self as searched
            *self.candidates.get_mut(&candidate.0).unwrap() = true;
            
            //Add all not already found nodes to the candidate list
            if let Some(neighbors) = self.matrix.weak_neighbors(&candidate.0){
                for n in neighbors{
                    if !self.candidates.contains_key(&n){
                        self.candidates.insert(n, false);
                    }
                }
            }
            
            //Now return candidate
            return Some(candidate.0);
        }else{
            //Apperently we found all
            return None;
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        //We have "at least" the number of unused candidates
        (self.candidates.iter().fold(0, |t, a| if !a.1{t+1}else{t}), None)
    }
}

#[cfg(test)]
mod mgraphtests{
    use crate::state_graph::ConnectionMatrix;

    #[test]
    fn access_indexes(){
        assert!(ConnectionMatrix::access_idx(0) == 0);
        assert!(ConnectionMatrix::access_idx(1) == 2);
        assert!(ConnectionMatrix::access_idx(2) == 5);
        assert!(ConnectionMatrix::access_idx(3) == 9);
        assert!(ConnectionMatrix::access_idx(4) == 14);
    }    
}

