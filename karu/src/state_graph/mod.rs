/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/

//!# State Graph
//!
//! The state graph is backed internatly by an adjacencj matrix, that tracks the connections of nodes.
//! A state can be accessed by its strong `StateId`, or you can try to access it with an `WeakStateId`. 
//! The former one is guaranteed to exist, the latter one might not exist (anymore).
//!

mod graph;
pub use graph::{State, StateId, StateGraph, GraphError};
mod matrix_graph;
pub use matrix_graph::{ConnectionMatrix, NodeId, MatrixError};
