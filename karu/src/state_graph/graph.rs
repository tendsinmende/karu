/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/

//! #Graph
//!
//! The graph part of karu handles nodes within a data graph.
//! A node has one or more states. Each state can be changed either by the node itself,
//! or if any connected state changes.

use std::{any::{Any, TypeId}, fmt::Display, time::Instant, usize};

use crate::state_graph::{ConnectionMatrix, MatrixError, NodeId, matrix_graph::WeakNodeId};

#[derive(Debug, PartialEq, Eq)]
pub enum GraphError{
    ///Types do not match. Contains type that was expected.
    TypeMissmatch(TypeId),
    EdgeDidNotExist,
    StateDidNotExist,
    OneAndTwoAreTheSame,
}

impl From<MatrixError> for GraphError{
    fn from(e: MatrixError) -> Self {
        match e{
            MatrixError::CanNotDisconnectSelf => GraphError::OneAndTwoAreTheSame,
            MatrixError::IdDitNotExist(_id) => GraphError::StateDidNotExist,
            MatrixError::SlotOutOfBounds => GraphError::StateDidNotExist
        }
    }
}

///Internal trait to make anys cloneable.
trait AnyClone: Any{
    fn clone_into_any(&self) -> Box<dyn AnyClone + Send + 'static>;
    fn as_any_ref(&self) -> &dyn Any;
    fn as_any_mut(&mut self) -> &mut dyn Any;
}


impl<T: Any + Clone + Send + 'static> AnyClone for T{
    fn clone_into_any(&self) -> Box<dyn AnyClone + Send + 'static> {
        Box::new(self.clone())
    }

    fn as_any_ref(&self) -> &dyn Any{
        &*self
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        &mut *self
    }
}

///Id of some state in a `StateGraph`.
pub struct StateId{
    nodeid: NodeId
}

impl core::fmt::Debug for StateId{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        <Self as Display>::fmt(self, f)
    }
}

impl Display for StateId{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "StateId({})", self.nodeid.location)
    }
}

impl StateId{
    pub fn as_weak(&self) -> WeakStateId{
        WeakStateId{
            nodeid: self.nodeid.as_weak()
        }
    }
}
///Weak version of an StateId. Is not guaranteed to exist.
#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq)]
pub struct WeakStateId{
    nodeid: WeakNodeId
}

///Actual state with erased type information. Use the downcast methodes to try and downcast it to its actual type.
pub struct State{
    tag: Instant,
    value: Box<dyn AnyClone + Send + 'static>
}

impl Clone for State{
    fn clone(&self) -> Self {
        State{
            tag: self.tag.clone(),
            value: self.value.clone_into_any()
        }
    }
}

impl State{
    pub fn downcast_ref<T: Clone + Send + 'static>(&self) -> Option<&T>{
        self.value.as_any_ref().downcast_ref()
    }
    pub fn downcast_mut<T: Clone + Send + 'static>(&mut self) -> Option<&mut T>{
        self.value.as_any_mut().downcast_mut()
    }
    pub fn as_any_ref(&self) -> &dyn Any{
        self.value.as_any_ref()
    }

    pub fn as_any_mut(&mut self) -> &mut dyn Any {
        self.value.as_any_mut()
    }

    fn merge(&mut self, other: State){
        //Overwrite self with other, if other is newer.
        if self.tag < other.tag{
            *self = other;
        }
    }
}

///Different versions of states. When rerouted, the actual state can be found at "Rerouted"'s usize in the state table
enum StateType{
    Rerouted(usize),
    Own(State),
    None
}

impl StateType{
    fn is_own(&self) -> bool{
        if let StateType::Own(_) = self{
            true
        }else{
            false
        }
    }

    ///unwraps self into state, panics if self is not owned
    fn unwrap_state(&self) -> &State{
        if let StateType::Own(s) = self{
            s
        }else{
            panic!("unwrap_state failed, was not owned")
        }
    }
}

impl core::fmt::Debug for StateType{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        <Self as Display>::fmt(self, f)
    }
}

impl Display for StateType{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self{
            StateType::None => write!(f, "None"),
            StateType::Own(_) => write!(f, "Own"),
            StateType::Rerouted(at) => write!(f, "Reroute(to: {})", at)
        }
    }
}

pub struct StateGraph{
    ///Saves stat information
    state_table: Vec<StateType>,
    ///Saves graph information
    connections: ConnectionMatrix,
}

impl StateGraph{
    pub fn new() -> Self{
        StateGraph{
            connections: ConnectionMatrix::new(),
            state_table: Vec::new(),
        }
    }

    ///Initializes the graph making sure that at least `capcaity` nodes and states can be created without reallocating.
    pub fn with_capacity(capacity: usize) -> Self{
        StateGraph{
            connections: ConnectionMatrix::with_capacity(capacity),
            state_table: Vec::with_capacity(capacity),
        }
    }
    
    ///Adds a new state to the graph.
    pub fn add<T: Clone + Send + 'static>(&mut self, value: T) -> StateId{

        let node = self.connections.alloc();        
        //Check if we can use this slot in the state table, otherwise alloc
        if node.location >= self.state_table.len(){
            self.state_table.reserve(node.location - self.state_table.len());
            while self.state_table.len() <= node.location{
                self.state_table.push(StateType::None);
            }
        }

        //Now check if the state at nodes location is owned, if that is the case, scan the whole state table
        //for any other nodes using this state.
        //if there are any, move the "owned" state to the first one, and reroute all following states to the found part of the old
        //subgraph
        if self.state_table[node.location].is_own(){
            //Swap the owned state and the None state
            let mut owned_state = StateType::None;
            core::mem::swap(&mut owned_state, &mut self.state_table[node.location]);
            debug_assert!(owned_state.is_own());
            
            let mut new_reroute = None;
            for (idx, n) in self.state_table.iter_mut().enumerate(){
                if let StateType::Rerouted(rr) = n{
                    if *rr == node.location{
                        //check if we did not find a reroute node yet
                        if new_reroute.is_none(){
                            new_reroute = Some(idx);
                            //Swap in owned state, all following nodes will point to this node
                            core::mem::swap(n, &mut &mut owned_state);
                        }else{
                            //Rereoute to the previously found route
                            *rr = new_reroute.unwrap();
                        }
                    }
                }
            }
        }
        
        self.state_table[node.location] = StateType::Own(State{
            tag: Instant::now(),
            value: Box::new(value)
        });
        
        StateId{
            nodeid: node
        }
    }
    
    
    pub fn get_state(&self, id: &StateId) -> &State{
        match &self.state_table[id.nodeid.location]{
            StateType::None => panic!("StateType of valid id was invalid. Should not happen, please report an issue for \"karu\" on gitlab.com/tendsinmende/karu"),
            StateType::Own(state) => state,
            StateType::Rerouted(at) => if let StateType::Own(s) = &self.state_table[*at]{
                s
            }else{
                panic!("StateType pointed at by rerout was not own. Should not happen.")
            }
        }
    }

    pub fn get_state_mut(&mut self, id: &StateId) -> &mut State{
        let id = match &self.state_table[id.nodeid.location]{
            StateType::None => panic!("StateType of valid id was invalid. Should not happen, please report an issue for \"karu\" on gitlab.com/tendsinmende/karu"),
            StateType::Own(_state) => id.nodeid.location,
            StateType::Rerouted(at) => *at,
        };

        if let StateType::Own(s) = &mut self.state_table[id]{
            s
        }else{
            panic!("StateType of valid id was invalid. Should not happen, please report an issue for \"karu\" on gitlab.com/tendsinmende/karu")
        }
    }

    pub fn get_value<T: Clone + Send + 'static>(&self, id: &StateId) -> Option<&T>{
        self.get_state(id).downcast_ref()
    }
    
    pub fn get_value_mut<T: Clone + Send + 'static>(&mut self, id: &StateId) -> Option<&mut T>{
        self.get_state_mut(id).downcast_mut()
    }

    fn get_actual_state_slot(&self, id: &StateId) -> usize{
        match &self.state_table[id.nodeid.location]{
            StateType::None => panic!("State was invalid. Please report an issue"),
            StateType::Own(_) => id.nodeid.location,
            StateType::Rerouted(at) => *at
        }
    }
    
    ///Tries to connect one state to another. Throws an error on runtime if the types of both states are not the same.
    pub fn connect(&mut self, one: &StateId, two: &StateId) -> Result<(), GraphError>{

        if one.nodeid.location == two.nodeid.location{
            return Err(GraphError::OneAndTwoAreTheSame);
        }
        
        //TODO: Currently we are just checking for a subgraph of one and two. We then merge both by searching for one and two's state id. When we got it.
        //we take the most recent one, and rerout one of the subgraphs to point to the new "most recent" state.
        //we also check that the types match.
        //however, this contains some overhead I guess. Check if there might be a more robust version sometime in the future.
        let state_of_one = self.get_actual_state_slot(one);
        let state_of_two = self.get_actual_state_slot(two);

        //Check that types match
        if self.state_table[state_of_one].unwrap_state().as_any_ref().type_id() != self.state_table[state_of_two].unwrap_state().as_any_ref().type_id(){
            return Err(GraphError::TypeMissmatch(self.state_table[state_of_one].unwrap_state().as_any_ref().type_id()));
        }
        
        debug_assert!(self.state_table[state_of_one].is_own());
        debug_assert!(self.state_table[state_of_two].is_own());
        
        //If the ids are the same, we can just connect since the must be in the same subgraph
        if state_of_one == state_of_two{
            self.connections.connect(&one.nodeid, &two.nodeid)?;
            Ok(())
        }else{
            //Are not in the same subgraph.therefore get all reachables of state two and reroute them to the main state of two. Then
            //connect
            let sg = self.connections.subgraph(&two.nodeid).collect::<Vec<_>>();
            for node in sg{
                //If `node` is the state owning node of two's subgraph. Update merge ones and twos subgraph state
                if node.0 == state_of_two{
                    let mut rr_state = StateType::Rerouted(state_of_one);
                    core::mem::swap(&mut rr_state, &mut self.state_table[node.0]);
                    debug_assert!(rr_state.is_own()); //Should now be two's old owned state

                    //Merge the two state
                    match (rr_state, &mut self.state_table[state_of_one]){
                        (StateType::Own(twos_state), StateType::Own(ones_state)) => ones_state.merge(twos_state),
                        _ => panic!("Encountered invalid state while merging")
                    }
                    
                }else{
                    //just rereoute
                    self.state_table[node.0] = StateType::Rerouted(state_of_one);
                }
                
            }
            self.connections.connect(&one.nodeid, &two.nodeid)?;
            Ok(())
        }
    }

    pub fn disconnect(&mut self, one: &StateId, two: &StateId) -> Result<(), GraphError>{

        //When disconnecting we first mark the disconnection in connection table.
        //after that we check if the got two different subgraphs. If thats the case, we
        //copy the "owned" state thats not reachable anymore to the "empty" subgraph.
        
        //mark as disconnected
        self.connections.disconnect(&one.nodeid, &two.nodeid)?;

        let sub_one = self.connections.subgraph(&one.nodeid).collect::<Vec<_>>();
        let in_subgraph = {
            let tid = two.nodeid.as_weak();
            let mut ret = false;
            for n in &sub_one{
                if n == &tid{
                    ret = true;
                    break;
                }
            }

            ret
        };
        
        if !in_subgraph{
            //Now find out in which subgraph the owned data is. Then copy the owned data to one of the other subgraphs nodes
            //and rewire all other nodes of that graph to point to the new oned data.

            let owned_id = self.get_actual_state_slot(one);
            
            if sub_one.contains(&WeakNodeId(owned_id)){
                //have to copy to second cluste
                //we assume that two cant own its data, since it was part of ones subgraph till just some time ago.
                debug_assert!(!self.state_table[two.nodeid.location].is_own());
                self.state_table[two.nodeid.location] = StateType::Own(self.state_table[owned_id].unwrap_state().clone());

                //now rewire all others of two
                for n in self.connections.subgraph(&two.nodeid){
                    if n == two.nodeid.as_weak(){
                        continue;
                    }else{
                        debug_assert!(!self.state_table[n.0].is_own());
                        self.state_table[n.0] = StateType::Rerouted(two.nodeid.location); //rereoute to new owned state index
                    }
                }
            }else{
                //have to copy to first cluster
                debug_assert!(!self.state_table[one.nodeid.location].is_own());
                self.state_table[one.nodeid.location] = StateType::Own(self.state_table[owned_id].unwrap_state().clone());

                //now rewire all others of two
                for n in self.connections.subgraph(&one.nodeid){
                    if n == one.nodeid.as_weak(){
                        continue;
                    }else{
                        debug_assert!(!self.state_table[n.0].is_own());
                        self.state_table[n.0] = StateType::Rerouted(one.nodeid.location); //rereoute to new owned state index
                    }
                }
            }
        }//else we are in the same subgraph. In that case we can just return since we dont need to move the "owned" datatype
        
        Ok(())
    }    

    pub fn get_subgraph(&self, id: &StateId) -> SubGraphIter{
        SubGraphIter{
            inner_iter: self.connections.subgraph(&id.nodeid)
        }
    }

    pub fn get_neighbors<'a>(&'a self, id: &StateId) -> NeighborIter<'a>{
        NeighborIter{
            inner_iter: self.connections.neighbors(&id.nodeid)
        }
    }
}


impl Display for StateGraph{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "\nStateGraph\n")?;
        self.connections.fmt(f)?;
        write!(f, "\nstate_table: {:?}", self.state_table)
    }
}

pub struct SubGraphIter<'a>{
    inner_iter: crate::state_graph::matrix_graph::SubGraphIter<'a>
}

impl<'a> Iterator for SubGraphIter<'a>{
    type Item = WeakStateId;
    fn next(&mut self) -> Option<Self::Item> {
        self.inner_iter.next().map(|i| WeakStateId{nodeid: i})
    }
}


pub struct NeighborIter<'a>{
    inner_iter: crate::state_graph::matrix_graph::IterNeighbors<'a>
}

impl<'a> Iterator for NeighborIter<'a>{
    type Item = WeakStateId;
    fn next(&mut self) -> Option<Self::Item> {
        self.inner_iter.next().map(|i| WeakStateId{nodeid: i})
    }
}
