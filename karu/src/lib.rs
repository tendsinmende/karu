/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/

///A state graph synchronises a state between several nodes in that graph.
///This is the internal data structure of karu-editor used to synchronise shared state of nodes.
pub mod state_graph;
