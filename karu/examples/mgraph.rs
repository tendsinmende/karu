/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
use karu::state_graph::ConnectionMatrix;



fn main(){
    let mut graph = ConnectionMatrix::new();
    let new0 = graph.alloc();
    let new1 = graph.alloc();
    let new2 = graph.alloc();
    let new3 = graph.alloc();
    
    println!("News: {}, {}, {}, {}", new0, new1, new2, new3);
    println!("{}", graph);

    graph.connect(&new0, &new1).unwrap();
    graph.connect(&new2, &new0).unwrap();
    graph.connect(&new3, &new1).unwrap();

    drop(new0);
    
    println!("{}", graph);

    let new4 = graph.alloc();
    graph.connect(&new4, &new3).unwrap();
    graph.connect(&new4, &new1).unwrap();
    graph.connect(&new3, &new2).unwrap();
    
    println!("alloced: {}\n{}", new4, graph);

    for i in graph.neighbors(&new1){
        println!("Neightbor {:?}", i);
    }
    
    for node in graph.subgraph(&new1){
        println!("SubGraph {:?}", node);
    }

    
    drop(new3);
    println!("{}", graph);
    let new5 = graph.alloc();
    println!("Alloced {}", new5);
    println!("{}", graph);    
    for node in graph.subgraph(&new5){
        println!("SubGraph {:?}", node);
    }
}
