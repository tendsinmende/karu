/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/

use karu::state_graph::{StateGraph, StateId};

struct Adder{
    ins: (StateId, StateId),
    out: StateId
}

impl Adder{
    fn exec(&self, graph: &mut StateGraph){
        let new = graph.get_value::<f32>(&self.ins.0).unwrap() + graph.get_value::<f32>(&self.ins.1).unwrap();
        *graph.get_value_mut::<f32>(&self.out).unwrap() = new;
    }
}


struct Subber{
    ins: (StateId, StateId),
    out: StateId
}

impl Subber{
    fn exec(&self, graph: &mut StateGraph){
        let new = graph.get_value::<f32>(&self.ins.0).unwrap() - graph.get_value::<f32>(&self.ins.1).unwrap();
        *graph.get_value_mut::<f32>(&self.out).unwrap() = new;
    }
}

fn main(){
    let mut graph = StateGraph::new();

    let add1 = Adder{
        ins: (graph.add(2.0f32), graph.add(2.0f32)),
        out: graph.add(42.0f32)
    };

    
    let sub = Subber{
        ins: (graph.add(10.0f32), graph.add(0.0f32)),
        out: graph.add(42.0f32)
    };

    graph.connect(&add1.out, &sub.ins.1).unwrap();

    add1.exec(&mut graph);

    println!("Add: {}", graph.get_value::<f32>(&add1.out).unwrap());

    sub.exec(&mut graph);

    println!("Subout: {}", graph.get_value::<f32>(&sub.out).unwrap());
    
    
}
