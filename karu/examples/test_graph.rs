/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
use karu::state_graph::StateGraph;



fn main(){
    let mut graph = StateGraph::new();
    
    let n0 = graph.add(0usize);
    let n1 = graph.add(1usize);
    let n2 = graph.add(2usize);
    let n3 = graph.add(3usize);
    let n4 = graph.add(4usize);
    let n5 = graph.add(5usize);

    graph.connect(&n0, &n1).unwrap();
    graph.connect(&n1, &n2).unwrap();
    graph.connect(&n0, &n2).unwrap();

    
    graph.connect(&n3, &n4).unwrap();
    graph.connect(&n3, &n5).unwrap();
    graph.connect(&n4, &n5).unwrap();

    println!("{}", graph);
    drop(n3);
    println!("{}", graph);
    let a0 = graph.add(6usize);
    println!("{}", graph);
    graph.connect(&n4, &a0).unwrap();
    println!("{}", graph);
    println!("---");
    graph.connect(&n5, &a0).unwrap();
    println!("{}", graph);
    graph.connect(&n2, &a0).unwrap();
    println!("{}", graph);
}
