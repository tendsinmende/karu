use std::any::TypeId;

use karu::state_graph::{StateGraph, StateId, GraphError};


fn complex_graph() -> (StateGraph, Vec<StateId>){
    let mut graph = StateGraph::new();

    //this graph:
    //
    //          a
    //        /  \
    //       b----c
    //      / \  / \
    //     d--e-f--g
    
    let a = graph.add(42usize);
    let b = graph.add(42usize);
    let c = graph.add(42usize);
    let d = graph.add(42usize);
    let e = graph.add(42usize);
    let f = graph.add(42usize);
    let g = graph.add(42usize);

    graph.connect(&a, &b).unwrap();
    graph.connect(&a, &c).unwrap();
    graph.connect(&b, &c).unwrap();
    
    graph.connect(&b, &d).unwrap();
    graph.connect(&b, &e).unwrap();
    graph.connect(&d, &e).unwrap();
    graph.connect(&e, &f).unwrap();
    graph.connect(&f, &c).unwrap();
    graph.connect(&f, &g).unwrap();
    graph.connect(&c, &g).unwrap();

    (graph, vec![a, b, c, d, e, f, g])
}


#[test]
fn ty_id_missmatch(){
    let (mut graph, ids) = complex_graph();

    let new = graph.add(42isize);
    let res = graph.connect(&ids[0], &new);
    assert!(res == Err(GraphError::TypeMissmatch(TypeId::of::<usize>())));
}

#[test]
fn insert(){
    let (mut graph, ids) = complex_graph();
    let new = graph.add(52usize);
    graph.connect(&new, &ids[0]).unwrap();
    //Since we did connect, should update whole graph
    assert!(graph.get_value::<usize>(&ids[0]).unwrap() == &52);
}

#[test]
fn disconnect(){
    let (mut graph, ids) = complex_graph();
    let new = graph.add(52usize);
    graph.connect(&ids[0], &new).unwrap();
    
    graph.disconnect(&ids[0], &ids[1]).unwrap();
    graph.disconnect(&ids[0], &ids[2]).unwrap();

    *graph.get_value_mut::<usize>(&new).unwrap() = 72;

    println!("Graph: {}", graph);
    
    assert!(graph.get_value::<usize>(&ids[1]).unwrap() == &52, "was not 52, but {} at idx: {:?}, connected where: {:?}", graph.get_value::<usize>(&ids[1]).unwrap(), ids[1], graph.get_subgraph(&ids[1]).collect::<Vec<_>>());
    assert!(graph.get_value::<usize>(&new).unwrap() == &72);
    assert!(graph.get_value::<usize>(&ids[0]).unwrap() == &72);
}
